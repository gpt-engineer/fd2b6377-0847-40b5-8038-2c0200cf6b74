document.getElementById('addTaskButton').addEventListener('click', function() {
    var taskInput = document.getElementById('taskInput');
    var taskList = document.getElementById('taskList');

    if (taskInput.value.trim() === '') {
        alert('Please enter a task.');
        return;
    }

    var li = document.createElement('li');
    li.className = 'flex justify-between items-center bg-white p-3 rounded shadow';

    var span = document.createElement('span');
    span.textContent = taskInput.value;
    li.appendChild(span);

    var checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    checkbox.className = 'form-checkbox h-5 w-5 text-indigo-600';
    checkbox.addEventListener('change', function() {
        span.style.textDecoration = this.checked ? 'line-through' : 'none';
    });
    li.appendChild(checkbox);

    var deleteButton = document.createElement('button');
    deleteButton.className = 'text-red-500 hover:text-red-700';
    deleteButton.innerHTML = '<i class="fas fa-trash"></i>';
    deleteButton.addEventListener('click', function() {
        taskList.removeChild(li);
    });
    li.appendChild(deleteButton);

    taskList.appendChild(li);

    taskInput.value = '';
});
